# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.0] - 02/12/2022
- Upgrade for PHP 8.1 compatibility.

## [2.0.6] - 08/12/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x`.

## [2.0.5] - 18/10/2021

- Add D9 readiness

## [2.0.4] - 11-06-2020

- Applied color changes from version 2.6.4 of CERN Theme (fixes colors of search overlay / components)

## [2.0.3] - 15-07-2019

- Applied color changes in order to keep color consistency in Display Formats

## [2.0.2] - 15-07-2019

- Fixed issue of CERN Logo not appearing in Teaser List component.

## [2.0.1] - 05-07-2019

- Fixed issue of double title for CERN override

## [2.0.0] - 12-02-2019

- Unecessary template files
