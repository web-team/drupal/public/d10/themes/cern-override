# CERN Override Theme

The CERN Override Theme is designed to give the ability to the Drupal 8 CERN Theme
users to override the CERN Theme.

## Getting Started

In order to Install theme:

1. Download the .zip file from the Git repository
2. Unzip the file
3. Mount locally your Drupal website by [following the official guidelines](http://information-technology.web.cern.ch/book/drupal-infrastructure-services/site-management/file-access)
4. Place the unzipped folder under the /themes folder of the mounted website
5. Visit Appearance page of your website
6. Find the CERN Override theme in the list of themes and click on "Set as default"

### Requirements

The only requirement to use the CERN Override theme is to have installed the CERN 
Theme.

## How to use

###

The CERN Override theme inherits the CERN Theme, and it is designed to make it
easy for the website admins to expand the CERN Theme and its functionalities. 
You can apply any changes to the CERN Theme like overriding or creating CSS/JS 
files, creating templates etc.

The file structure of the CERN Override is similar to any other Drupal theme. 
More specifically, after downloading the theme you will notice that the followed
structure looks like the image. As you will notice, the theme comes with several
predefined files: theme.css, colors folder etc. Those files are created in order
to override the already existing values so feel free to use them. The CERN 
override theme has bigger priority than the CERN theme meaning that if you apply
any change in the styling of the theme, it will override the CERN them.

### Creating your own css files
Additionally, to what already mentioned, you are free to create your own CSS files.
Especially in cases of websites that require numerous lines of styling code, 
you are highly advised to create your own file and structure the styling files 
based on your needs. If you want to create your own files, don't forget to 
declare them in the `.libraries.yml` file of the theme.

## Versioning

For the versions available, see the [tags on this repository](https://gitlab.com/web-team/drupal/public/d8/themes/cernoverride/tags).
